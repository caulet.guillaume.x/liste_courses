<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Liste de courses</title>
</head>
<h1>Liste de courses</h1>
<div class="list">
 
   <header class="flex">
    <div class="prod">Produit</div>
    <div class="qt">Qté</div>
    <div class="btn">Btn</div>
  </header>
 <!-- foreach... -->
   <div class="flex">
    <div class="prod">Chocolat</div>
    <div class="qt">4</div>
    <div class="btn">
      <form action="supp.php">
        <input type="hidden" value="0" name="id">
        <input type="submit" value="Suppr">
      </form>
    </div>
  </div>
 <!-- end foreach -->
  
  <!-- formulaire -->
  <form action="add.php" method="post">
    <div class="flex">
      <div class="prod">
        <input autocomplete="off" type="text" name="nom">
      </div>
      <div class="qt">
        <input type="number" name="qt">
      </div>
      <div class="btn">
        <input type="submit" value="OK">
      </div>
    </div>
  </form>
  
</div>

</body>
</html>
          