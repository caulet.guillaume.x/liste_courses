DROP DATABASE IF EXISTS course;


CREATE DATABASE course;

USE course; 

CREATE TABLE liste (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Produit VARCHAR(30) NOT NULL,
    Quantité VARCHAR(30) NOT NULL
    
);
GRANT ALL PRIVILEGES ON course.* TO 'admin'@'localhost';

INSERT INTO liste (id,produit,quantité) 
VALUES ("Produit","Quantité");

SELECT * FROM liste;
